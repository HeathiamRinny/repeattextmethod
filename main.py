from datetime import datetime


def EngApp(): 
    currentTime =datetime.now()
    currentTime = currentTime.hour
    print(currentTime)
    isTyping = False
    if(currentTime < 12):
        print("Good morning !")
    elif(currentTime < 18):
        print("Good afternoon !")
        isTyping = True
    else:
        print("Good evening !")
        isTyping = True

    while(isTyping):
        typed = input("What do you want to say ?")
        print(typed)
        isPalindromeEng(typed);
    

def isPalindromeEng(self:str) ->bool:
    tempText = self
    tempText = tempText[::-1]
    if(tempText.lower() == self.lower()):
        print("Well done !")
    

def FrenchApp(): 
    currentTime =datetime.now()
    currentTime = currentTime.hour*10000
    isTyping = False
    if(currentTime < 18):
        print("Bonjour !")
        isTyping = True
    else: 
        print("Bonsoir")
        isTyping = True

    while(isTyping):
        typed = input("Que veux tu écrire ?")
        print(typed)
        isPalindromeFrench(typed);
    

def isPalindromeFrench(self:str) ->bool:
    tempText = self
    tempText = tempText[::-1]
    if(tempText.lower() == self.lower()):
        print("Bien dis !")
    

def StartApp(): 
  lang = input("Choose your language (French or English)")
  if(lang==("French") or lang==("french") or lang==("français") or lang == ("Français")):
    FrenchApp()
  elif (lang==("English") or lang==("english")):
    EngApp()

StartApp()